package com.company;

import java.util.Scanner;

public class Card
{
    private Scanner scanner = new Scanner(System.in);
    enum Ulta
    {
        Regeneration,
        Armor,
        PoisonDamage,
        StoppingTime,
        FlameDamage,
        GlitchDamage,
        Default
    }
    private String name;
    private int health;
    private int damage;
    private Ulta ultaOfCard;

    public Card(String name, int health, int damage, Ulta ultaOfCard)
    {
        this.name = name;
        this.health = health;
        this.damage = damage;
        this.ultaOfCard = ultaOfCard;
    }

    public Card(Card card)
    {
        name = card.name;
        health = card.health;
        damage = card.damage;
        ultaOfCard = card.ultaOfCard;
    }

    public String getStringInfo()
    {
        return "Name: "+name+" Healt: "+health+" Damage: "+damage+" Ulta of card: "+ultaOfCard+".";
    }

    public String getName()
    {
        return name;
    }

    public int getHealth()
    {
        return health;
    }

    public int getDamage()
    {
        return damage;
    }

    public Ulta getUltaOfCard()
    {
        return ultaOfCard;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setHealth(int health)
    {
        if (health>0)
        {
            this.health = health;
        }
    }

    public void setDamage(int damage)
    {
        if (damage>0)
        {
            this.damage = damage;
        }
    }
}
