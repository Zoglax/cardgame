package com.company;

public class CardManager
{
    private int count;
    private Card[] cards;

    public CardManager(int count)
    {
        this.count = count;
    }

    public void PrintAll(Card[] cards)
    {
        if(cards==null)
        {
            System.out.println("Deck of cards is empty");
        }
        else if(count==0)
        {
            System.out.println("Deck of cards is empty");
        }
        else
        {
            for (int i=0;i<count;i++)
            {
                cards[i].getStringInfo();
            }
        }
    }
}
